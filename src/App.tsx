import React, { useReducer } from 'react';
import NS from './blocks/NS';
import AppContext from './data/AppContext';
import { appStateReducer, initialState } from './data/appReducer';

function App() {
	const [ state, dispatch ] = useReducer(appStateReducer, initialState);
	
	return (
		<AppContext.Provider value={{ state, dispatch }}>
			<div style={{ borderBottom: '1px solid #000', margin: '1em' }}>
				<NS data={state.blocks} parent={undefined} />
			</div>
		</AppContext.Provider>
	);
}

export default App;
