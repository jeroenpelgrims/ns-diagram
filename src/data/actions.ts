type BlockAction = { blockId: string; };

export type SetBlockTextAction = BlockAction & {
    type: 'SET_BLOCK_TEXT';
    name: string;
};

export type SetBlockTextEditingAction = {
    type: 'SET_BLOCK_TEXT_EDITING';
    blockId: string | undefined;
};

export type AddAction = { type: 'ADD' };
export type SubstractAction = { type: 'SUBSTRACT' };

export type AppAction =
    | SetBlockTextAction
    | SetBlockTextEditingAction
    | AddAction
    | SubstractAction;