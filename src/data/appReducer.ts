import { AppAction } from "./actions";
import { Block, BranchPath } from "../types";
import data from './example_data';

export type AppState = {
	editableTextBlockId: string | undefined;
	blocks: Block[];
};

export const initialState: AppState = {
	editableTextBlockId: undefined,
	blocks: data
};

function mutateBlock(
	blocks: Block[],
	blockId: string,
	mutation: <T extends Block | BranchPath>(block: T) => T
): Block[] {
	return blocks.map(block => {
		if (block.id === blockId) {
			return mutation(block);
		} else {
			// Go deeper and mutate the children
			switch (block.operation) {
				case 'process':
					return block;
				case 'loop':
					const children = mutateBlock(block.children, blockId, mutation);
					return { ...block, children };
				case 'branch':
					const branches = block.branches.map(branchPath => {
						// If one of the branchPaths (the options)
						// is the block to be mutated, mutate it
						// Else mutate the option's children
						if (branchPath.id === blockId) {
							return mutation(branchPath);
						} else {
							return {
								...branchPath,
								children: mutateBlock(
									branchPath.children,
									blockId,
									mutation
								)
							};
						}
					});
					return { ...block, branches };
				default:
					return block;
			}
		}
	});
}

export function appStateReducer(state: AppState, action: AppAction) {
	switch (action.type) {
		case 'SET_BLOCK_TEXT':
			return {
				...state,
				blocks: mutateBlock(
					state.blocks,
					action.blockId,
					block => {
						switch (block.operation) {
							case 'process':
								return { ...block, name: action.name };
							case 'loop':
								return { ...block, condition: action.name };
							case 'branch':
								return { ...block, test: action.name };
							case undefined:
								return block;
							case 'branchpath':
								return { ...block, condition: action.name };
						}
					}
				)
			};
		case 'SET_BLOCK_TEXT_EDITING':
			return { ...state, editableTextBlockId: action.blockId }
		case 'ADD':
			// return { ...state, count: state.count + 1 };
		case 'SUBSTRACT':
			// return { ...state, count: state.count - 1 };
	}
	
	return state;
}