import React from 'react';
import { AppState, initialState } from './appReducer';
import { AppAction } from './actions';

type AppContext = {
    state: AppState,
    dispatch: React.Dispatch<AppAction>
};

export default React.createContext<AppContext>({
    state: initialState,
    dispatch: () => null
});