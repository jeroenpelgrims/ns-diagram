import { v4 as uuid } from 'uuid';
import { Block } from "../types";

const data: Block[] = [
	{ id: uuid().toString(), operation: 'process', name: "Take a trip" },
	{ id: uuid().toString(), operation: undefined },
	{
		id: uuid().toString(),
		operation: 'branch',
		test: "Fly or drive?",
		branches: [
			{
				id: uuid().toString(),
				operation: 'branchpath',
				condition: "Fly",
				children: [
					{ id: uuid().toString(), operation: 'process', name: "Go to airport" },
					{ id: uuid().toString(), operation: 'process', name: "Go trough security" },
					{
						id: uuid().toString(),
						operation: 'loop',
						condition: "For each layover",
						pre: true,
						children: [
							{ id: uuid().toString(), operation: 'process', name: "Get on plane" },
							{ id: uuid().toString(), operation: 'process', name: "Ride the clouds" }
						]
					},
				]
			},
			{
				id: uuid().toString(),
				operation: 'branchpath',
				condition: "Drive",
				children: [
					{ id: uuid().toString(), operation: 'process', name: "Start road trip" },
					{
						id: uuid().toString(),
						operation: "loop",
						condition: "While not there",
						pre: false,
						children: [
							{
								id: uuid().toString(),
								operation: 'branch',
								test: "Status",
								branches: [
									{
										id: uuid().toString(),
										operation: 'branchpath',
										condition: "Low gas",
										children: [
											{ id: uuid().toString(), operation: 'process', name: "Fill up" }
										]
									},
									{
										id: uuid().toString(),
										operation: 'branchpath',
										condition: "Hungry",
										children: [
											{ id: uuid().toString(), operation: 'process', name: "Eat" }
										]
									},
									{
										id: uuid().toString(),
										operation: 'branchpath',
										condition: "Normal",
										children: [
											{ id: uuid().toString(), operation: 'process', name: "Drive" }
										]
									}
								]
							},
							{
								id: uuid().toString(),
								operation: 'branch',
								test: "Status",
								branches: [
									{
										id: uuid().toString(),
										operation: 'branchpath',
										condition: "Low gas",
										children: [
											{ id: uuid().toString(), operation: 'process', name: "Fill up" }
										]
									},
									{
										id: uuid().toString(),
										operation: 'branchpath',
										condition: "Hungry",
										children: [
											{ id: uuid().toString(), operation: 'process', name: "Eat" }
										]
									},
									{
										id: uuid().toString(),
										operation: 'branchpath',
										condition: "Normal",
										children: [
											{ id: uuid().toString(), operation: 'process', name: "Drive" }
										]
									},
									{
										id: uuid().toString(),
										operation: 'branchpath',
										condition: "Baby crying",
										children: [
											{ id: uuid().toString(), operation: 'process', name: "Refresh pamper" }
										]
									}
								]
							}
						]
					}
				]
			}
		]
	},
	{ id: uuid().toString(), operation: 'process', name: "Arrive at destination" }
];

export default data;