import React from 'react';
import { Block } from "../types";
import Process from './Process';
import Loop from './Loop';
import Branch from './Branch';
import UndefinedBlock from './UndefinedBlock';
import { BaseBlockProps } from './Block';

type NSProps = {
	data: Block[];
} & BaseBlockProps;

export default function NS({ data, parent }: NSProps) {
	return (
		<div style={{
			flex: 1,
			display: 'flex',
			flexDirection: 'column'
		}}>
			{data.map((block, instructionIndex) => {
				switch (block.operation) {
					case "process":
						return <Process {...block} parent={parent} instructionIndex={instructionIndex} />
					case "loop":
						return <Loop {...block} parent={parent} instructionIndex={instructionIndex} />
					case "branch":
						return <Branch {...block} parent={parent} instructionIndex={instructionIndex} />
					case undefined:
						return <UndefinedBlock parent={parent} />
					default:
						return null;
				}
			})}
		</div>
	);
}