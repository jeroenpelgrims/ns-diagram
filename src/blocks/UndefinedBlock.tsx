import React from 'react';
import Block, { BaseBlockProps } from './Block';

export default function UndefinedBlockComponent({ parent }: BaseBlockProps) {
	return (
		<Block parent={parent}>
			Undefined block
		</Block>
	);
}