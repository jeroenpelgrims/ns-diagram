import React from 'react';
import { Block } from "../types";

export type BaseBlockProps = {
	parent: Block | undefined;
	instructionIndex?: number;
};

function style(props: BaseBlockProps) {
	const { parent, instructionIndex } = props;
	const postLoopParent = !!parent && parent.operation === 'loop' && !parent.pre && instructionIndex === 0;
	return {
		flex: 1,
		border: '1px solid #000',
		borderBottom: 'none',

		...(parent ? {
			borderLeft: 'none',
			borderRight: 'none'
		} : {}),

		...(postLoopParent ? {
			borderTop: 'none'
		} : {})
	};
}

export default function BlockComponent(props: React.PropsWithChildren<BaseBlockProps>) {
	return (
		<div style={style(props)}>
			{props.children}
		</div>
	);
}