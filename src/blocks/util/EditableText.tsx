import React, { useState, useContext } from 'react';
import AppContext from '../../data/AppContext';
import { SetBlockTextAction } from '../../data/actions';

type EditableTextProps = {
	text: string;
	blockId: string;
};

export default function EditableText({ text, blockId }: EditableTextProps) {
	const { state, dispatch } = useContext(AppContext);
	const [ stateText, setStateText ] = useState(text);
	const editing = state.editableTextBlockId === blockId;

	function handleKey(e: React.KeyboardEvent<HTMLInputElement>) {
		const key_enter = 13;
		const key_esc = 27;
		
		if (e.keyCode === key_esc) {
			dispatch({ type: "SET_BLOCK_TEXT_EDITING", blockId: undefined });
			setStateText(text);
		}

		if (e.keyCode === key_enter) {
			dispatch({ type: "SET_BLOCK_TEXT_EDITING", blockId: undefined });
			const action: SetBlockTextAction = {
				type: 'SET_BLOCK_TEXT',
				blockId,
				name: stateText
			};
			dispatch(action);
		}
	}

	if (editing) {
		return (
			<input
				autoFocus
				type="text"
				value={stateText}
				onChange={(e) => setStateText(e.target.value)}
				onKeyUp={handleKey}
			/>
		);
	}

	return (
		<span onClick={
			() => dispatch({
				type: "SET_BLOCK_TEXT_EDITING",
				blockId
			})
		}>
			{text}
		</span>
	);
}