import React from 'react';
import Block, { BaseBlockProps } from './Block';
import { Loop } from '../types';
import NS from './NS';
import EditableText from './util/EditableText';

function style(pre: boolean) {
	return {
		...(pre ? { padding: '0.5em 0 0 1em' } : {}),
		...(!pre ? { padding: '0em 0 0.5em 1em' } : {})
	};
}

export default function LoopComponent(props: BaseBlockProps & Loop) {
	return (
		<Block parent={props.parent}>			
			<div style={style(props.pre)}>

				{props.pre ? (
					<div style={{ marginBottom: '0.5em' }}>
						<EditableText text={props.condition} blockId={props.id} />
					</div>
				) : null}
				
				<div style={{
					display: 'flex',
					borderLeft: '1px solid #000',
					...(!props.pre ? { borderBottom: '1px solid #000' } : {})
				}}>
					<NS data={props.children} parent={props} />
				</div>
				
				{!props.pre? (
					<div style={{ marginTop: '0.5em' }}>
						<EditableText text={props.condition} blockId={props.id} />
					</div>
				): null}
			</div>
		</Block>
	);
}