import React from 'react';
import Block, { BaseBlockProps } from './Block';
import { Branch, BranchPath } from '../types';
import EditableText from './util/EditableText';
import NS from './NS';

const flexContainer = {
	display: 'flex'
}

const branchStyle = {
	...flexContainer,
	flex: 1,
	borderRight: '1px solid #000'
};

const branchHeaderStyle = {
	display: 'block',
	textAlign: 'center' as const,
	paddingTop: '1em'
}

function renderBranchHeader(branch: BranchPath, isLast: boolean) {
	return (
		<div key={branch.id} style={{
			...branchStyle,
			...branchHeaderStyle,
			...(isLast ? { borderRight: 'none' } : {})
		}}>
			<EditableText text={branch.condition} blockId={branch.id} />
		</div>
	);
}

function renderBranchPath(branch: BranchPath, parent: Branch, isLast: boolean) {
	return (
		<div key={branch.id} style={{
			...branchStyle,
			...(isLast ? { borderRight: 'none' } : {})
		}}>
			<NS data={branch.children} parent={parent} />
		</div>
	);
}

export default function BranchComponent(props: Branch & BaseBlockProps) {
	const percentOptions = (props.branches.length - 1) / props.branches.length;
	const percentOther = 1 - percentOptions;

	return (
		<Block parent={props.parent} instructionIndex={props.instructionIndex}>
			<div>
				<header>
					<div style={{
						height: '3em',
						display: 'flex',
						backgroundColor: '#fff',
					}}>
						<div style={{ width: `${percentOptions * 100}%`, background: 'linear-gradient(to top right, rgba(255, 255, 255, 0) calc(50% - 1px), #000, rgba(255, 255, 255, 0) calc(50% + 1px))' }}>&nbsp;</div>
						<div style={{ width: `${percentOther * 100}%`, background: 'linear-gradient(to bottom right, rgba(255, 255, 255, 0) calc(50% - 1px), #000, rgba(255, 255, 255, 0) calc(50% + 1px))' }}>&nbsp;</div>
					</div>
					<div style={{
						textAlign: 'center' as const,
						backgroundColor: '#fff',
						marginTop: '-2.5em',
						marginLeft: `${(percentOptions-0.5) * 100 * 2}%`
					}}>
						<EditableText text={props.test} blockId={props.id} />
					</div>
				</header>
				
				<div style={flexContainer}>
					{props.branches.map((branch, i) => renderBranchHeader(branch, i === props.branches.length - 1))}
				</div>
				
				<div style={flexContainer}>
					{props.branches.map((branch, i) => renderBranchPath(branch, props, i === props.branches.length - 1))}
				</div>
			</div>
		</Block>
	);
}