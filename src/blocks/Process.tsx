import React from 'react';
import { Process } from '../types';
import Block, { BaseBlockProps } from './Block';
import EditableText from './util/EditableText';

const style = {
	padding: '0.5em 1em',
	textAlign: 'center' as const
};

export default function ProcessComponent({ id, name, parent }: Process & BaseBlockProps) {
	return (
		<Block parent={parent}>
			<div style={style}>
				<EditableText text={name} blockId={id} />
			</div>
		</Block>
	);
}