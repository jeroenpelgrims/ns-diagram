type BaseBlockProps = {
	id: string;
}

export type Process = {
	operation: 'process';
	name: string;
} & BaseBlockProps;

export type Loop = {
	operation: 'loop';
	condition: string;
	pre: boolean;
	children: Block[];
} & BaseBlockProps;

export type Branch = {
	operation: 'branch';
	test: string;
	branches: BranchPath[];
} & BaseBlockProps;

export type BranchPath = {
	operation: 'branchpath';
	condition: string;
	children: Block[];
} & BaseBlockProps;

export type UndefinedBlock = {
	operation: undefined;
} & BaseBlockProps;

export type Block = Process | Loop | Branch | UndefinedBlock;